package programmes;

import java.util.Scanner;

public class Exo11 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int prop, rand=0, nbE=1;
        float seconds=0.0f;
        rand=(int)(Math.random()*999+1);
        long tempsDebut=System.currentTimeMillis();
        
        System.out.println("Je viens de penser à un nombre entre 1 et 1000");
        System.out.println("Essayez de le deviner avec le moins d'essais possible!");
        System.out.println("Vous pouvez abandonner en saisissant la valeur 0");
        System.out.println("");
        
        System.out.println("Quel nombre proposez-vous ? ");
        prop=scan.nextInt();
        
        while (prop != 0 && prop!=rand ) {
            if (prop>rand) {
                System.out.println("Trop grand!");
            }
            if (prop<rand) {
                System.out.println("Trop petit!");
            }
            System.out.println("Quel nombre proposez-vous ? ");
            prop=scan.nextInt();
            nbE++;
        }
        
        long tempsFin=System.currentTimeMillis();
        seconds=(tempsFin-tempsDebut)/1000F;
        
        if (prop==rand) {
            System.out.println("Vous avez gagné, le nombre a deviné était bien : "+rand);
            System.out.println("Il vous a fallu "+nbE+" essais et "+seconds+" secondes pour trouver le nombre caché!");
        }
        if (prop==0) {
            System.out.println("Vous avez choisi d'abandonner, le chiffre a deviné était : "+rand);
            System.out.println("Vous avez abandonné au bout de "+nbE+" essais et "+seconds+" secondes");
        }
        
    }
}
