package programmes;

import java.util.Scanner;

public class Exo7 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int age, cotisation=0, assurance=80;
        float total=0.0f;
        String paiement;
        
        System.out.print("Veuillez indiquer votre âge : ");
        age=scan.nextInt();
        
        if (age>0 && age<=8) {
            cotisation=45;
        } else if (age>=9 && age<=12) {
            cotisation=60;
        } else if (age>=13 && age<=15) {
            cotisation=75;
        } else {
            cotisation=90;
        }
        
        total=cotisation+assurance;
        
        System.out.println("Le prix de l'inscription est de : "+cotisation+"€");
        System.out.println("le prix de 'lassurance est de : "+assurance+"€");
        System.out.println("Total : "+total+"€");
        
        System.out.print("Souhaitez-vous payer en 3 fois ? ");
        paiement=scan.next();

        if(paiement.equals("oui")) {
            System.out.println("Vous payez en trois fois, le 15 septembre, le 15 janvier et le 15 avril, la somme est de : "+total/3+"€");  
        } else {
            System.out.println("Vous payer en une fois le 15 septembre la somme de : "+total+"€");
        }
    }
}
