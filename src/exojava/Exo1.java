package programmes;

import java.util.Scanner;

public class Exo1 {
    public static void main (String[] args) {
        float lon, lar, peri, surf;
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Veuillez entrer une longueur : ");
        lon = scan.nextFloat();
        System.out.print("Veuillez entrer une largeur : ");
        lar = scan.nextFloat();
        
        peri= lon*2 + lar*2;
        surf= lon*lar;
        
        System.out.println("Le périmètre vaut : " + peri);
        System.out.println("La surface vaut : " + surf);
    }
}
