package programmes;

import java.util.Scanner;

public class Exo8 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        float salaire=0.0f, primeB=0.0f, primeE=0.0f, primeA=0.0f, primeV=0.0f;
        int enf=0, ancien=0;
        
        System.out.println("Calcul de votre prime de vacances :");
        
        System.out.print("Quel est votre salaire brut mensuel en Euros ? ");
        salaire=scan.nextFloat();
        
        System.out.print("Combien avez-vous d'enfants de moins de 18 ans ? ");
        enf=scan.nextInt();
        
        System.out.print("Combien avez-vous d'années d'ancienneté ? ");
        ancien=scan.nextInt();
        
        if (salaire>0 && salaire<=1600) {
            primeB=180;
        } else if (salaire>1600 && salaire<=2200) {
            primeB=150;
        } else if (salaire>2200 && salaire<=3000) {
            primeB=100;
        } else if (salaire>3000) {
            primeB=60;
        } else {
            System.out.println("Entrer une valeur de salaire valable!");
        }
        
        primeE=enf*110;
        
        if (ancien>=1) {
            primeA=90;
        } else if (ancien>=5) {
            primeA=120;
        } else if (ancien>=10) {
            primeA=150;
        } else {
            System.out.println("Entrer une valeur d'ancienneté valable!");
        }
        
        System.out.println("Prime de base : "+ primeB + "€");
        System.out.println("Prime enfants : "+ primeE + "€");
        System.out.println("Prime d'anciennté : "+ primeA + "€");
        
        primeV=primeA+primeE+primeB;
        System.out.println("Votre prime de vacances s'élève à : "+ primeV +"€");
    }
}
