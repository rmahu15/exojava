package programmes;

import java.util.Scanner;

public class Exo10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int taille, age, morpho;
        String sexe;
        float poidsL=0.0f, poidsC=0.0f;
                
        System.out.println("Calcul de votre poids idéal :");
        
        System.out.print("Quelle est votre taille en cm ? ");
        taille=scan.nextInt();
        
        System.out.print("Quel est votre age ? ");
        age=scan.nextInt();
        
        System.out.print("Quel est votre sexe (F ou M) ? ");
        sexe=scan.next();
        
        System.out.print("Quelle est votre morphologie (1: Fine | 2: Normale | 3: Large) ? ");
        morpho=scan.nextInt();
        
        if (sexe.equals("F")) {
            poidsL=taille-100-(taille-150)/2.5f;
        } else if (sexe.equals("M")) {
            poidsL=taille-100-(taille-150)/4;
        } else {
            System.out.println("Entrer une valeur de sexe valable!");
        }
        
        if (morpho==1) {
            poidsC=(taille-100+age/10)*0.9f*0.9f;
        } else if (morpho==2) {
            poidsC=(taille-100+age/10)*0.9f;
        } else if (morpho==3) {
            poidsC=(taille-100+age/10)*0.9f*1.1f;
        } else {
            System.out.println("Entrer une valeur de morphologie valable!");
        }
        
        System.out.println("Poids idéal selon la formule de Lorentz : "+poidsL+"kg.");
        System.out.println("Poids idéal selon la formule de Creff : "+poidsC+"kg.");
    }
}
