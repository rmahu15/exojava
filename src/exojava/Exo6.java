package programmes;

import java.util.Scanner;

public class Exo6 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int nbAdu=0, nbEnf=0, nbEtu=0;
        String promo;
        double total=0.0 , moy=0.0;
        
        System.out.print("Nombres d'adultes : ");
        nbAdu=scan.nextInt();
        
        System.out.print("Nombres d'Enfants : ");
        nbEnf=scan.nextInt();
        
        System.out.print("Nombres d'Etudiants : ");
        nbEtu=scan.nextInt();
        
        System.out.print("Jour de promotion ? (répondre par oui ou par non) : ");
        promo=scan.next();
        
        if (promo.equals("oui")) {
            total=(nbAdu*7 + nbEnf*4 + nbEtu*5.5)*0.8;
        } else {
            total=nbAdu*7 + nbEnf*4 + nbEtu*5.5;
        }

        System.out.println("Montant dû : "+total+"€");
        
        moy=total/(nbAdu+nbEnf+nbEtu);
        System.out.println("Prix moyen d'une place : "+moy+"€");
    }
}
